import time
import socket
import sys
import threading
import multiprocessing
put_request = "POST /data.json HTTP/1.1\r\nHost: localhost:90\r\nConnection: keep-alive\r\nUser-Agent: Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.102 Safari/537.36\r\nContent-Type: application/x-www-form-urlencoded\r\nContent-Length: 300\r\nAccept-Encoding: gzip, deflate, br\r\nAccept-Language: en-GB,en-US;q=0.9,en;q=0.8\r\n\r\nname=Jatish&password=1234"

get_request =   "GET /index.html HTTP/1.1\r\nHost: localhost:90\r\nConnection: keep-alive\r\nUpgrade-Insecure-Requests: 1\r\nUser-Agent: Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.102 Safari/537.36\r\nAccept: text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9\r\nIf-Unmodified-Since: Wed, 23 Oct 2030 22:15:29 GMT\r\nPurpose: prefetch\r\nSec-Fetch-Site: none\r\nSec-Fetch-Mode: navigate\r\nSec-Fetch-User: ?2\r\nSec-Fetch-Dest: document\r\nAccept-Encoding: gzip, deflate, br\r\nAccept-Language: en-GB,en-US;q=0.9,en;q=0.8\r\n\r\n"

bad_request = "WAIT /index.html HTTP/1.1\r\nHost: localhost\r\n\r\n"

get_form = "GET /bdf HTTP/1.1\r\nHost: localhost:90\r\nConnection: keep-alive\r\nUpgrade-Insecure-Requests: 1\r\nUser-Agent: Mozilla/5.0 (X11; Linux x86    _64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.102 Safari/537.36\r\nAccept: text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/we    bp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9\r\nPurpose: prefetch\r\nSec-Fetch-Site: none\r\nSec-Fetch-Mode: navigate\r\nSec-Fetch-User: ?2\r\nSe    c-Fetch-Dest: document\r\nAccept-Encoding: gzip, deflate, br\r\nAccept-Language: en-GB,en-US;q=0.9,en;q=0.8\r\n\r\n"

put_request1 = "PUT /icons/second.html HTTP/1.1\r\nHost: localhost:90\r\nConnection: keep-alive\r\nUser-Agent: Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.102 Safari/537.36\r\nAccept: application/x-www-form-urlencoded\r\nAccept-Encoding: gzip, deflate, br\r\nAccept-Language: en-GB,en-US;q=0.9,en;q=0.8\r\n\r\n<h1>Hello World</h1>"

post_form ="""POST /icons/data.json HTTP/1.1\r\nHost: localhost:90\r\nUser-Agent: Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0\r\nAccept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,/;q=0.8\r\nAccept-Language: en-US,en;q=0.5\r\nAccept-Encoding: gzip, deflate\r\nContent-Type: multipart/form-data; boundary=-----------------------------6989595983727954435922176565\r\nContent-Length: 521\r\nOrigin: http://localhost:90\nConnection: keep-alive\r\nReferer: http://localhost:90/form.html\nUpgrade-Insecure-Requests: 1\r\n\r\n-----------------------------6989595983727954435922176565\r\nContent-Disposition: form-data; name="Name"\r\n\r\nJatish\r\n-----------------------------6989595983727954435922176565\r\nContent-Disposition: form-data; name="Yea"\r\n\r\n2022\r\n-----------------------------6989595983727954435922176565\r\nContent-Disposition: form-data; name="Branch"; filename="example.txt"\r\n\r\nComputer\r\n-----------------------------6989595983727954435922176565\r\nContent-Disposition: form-data; name="MIS"\r\n\r\n111803058\r\n-----------------------------6989595983727954435922176565\r\n"""

img = open("img.jpg", 'rb')
img_data = img.read()
put_req ="PUT /icons.html HTTP/1.1\r\nHost: localhost:90\r\nConnection: keep-alive\r\nUser-Agent: Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.102 Safari/537.36\r\nAccept: application/x-www-form-urlencoded\r\nAccept-Encoding: gzip, deflate, br\r\nContent-Length : 552\r\nAccept-Language: en-GB,en-US;q=0.9,en;q=0.8\r\n\r\n<h1>Hello World</h1>"
put_req = put_req.encode()
#put_req += put_req + b'\r\n' + img_data + b'\r\n'

print(sys.getsizeof(put_req))
print(put_req)
serverPort = int(sys.argv[1])
serverName = '127.0.0.1'
def client_send(msg) :
    clientSocket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    clientSocket.connect((serverName, serverPort))
    clientSocket.send(msg)
    print(clientSocket.recv(1024).decode())

"""jobs = []
threads = 5
for _ in range(threads) :
    thread = multiprocessing.Process(target = client_send, args=(get_request, ))
    jobs.append(thread)

for j in jobs :
    j.start()

for j in jobs :
    j.join()
"""
jobs = []
for _ in range(1) :
    #jobs.append(get_request)
    jobs.append(put_request.encode())
    
if __name__ == '__main__' :
    with multiprocessing.Pool(10) as p :
        p.map(client_send, jobs)
