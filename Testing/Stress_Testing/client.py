import json
import sys
import socket
import multiprocessing
jobs = []
with open("../Requests/get_requests.json", "r") as f :
    req = json.load(f)
for r in req :
    jobs.append(req[r])

with open("../Requests/put_requests.json", "r") as f :
    req = json.load(f)
for r in req :
    jobs.append(req[r])

with open("../Requests/post_requests.json", "r") as f :
    req = json.load(f)
for r in req :
    jobs.append(req[r])

with open("../Requests/head_requests.json", "r") as f :
    req = json.load(f)
for r in req :
    jobs.append(req[r])

with open("../Requests/delete_requests.json", "r") as f :
    req = json.load(f)
for r in req :
    jobs.append(req[r])

jobs = jobs * 100
serverport = 9000
servername = '127.0.0.1'

def client_send(msg) :
    print(r)
    print(req[r])
    client_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    client_socket.connect((servername, serverport))
    client_socket.send(msg.encode())
    print(client_socket.recv(8192).decode())


if __name__ == '__main__' :
    with multiprocessing.Pool(10) as p :
        p.map(client_send, jobs)
