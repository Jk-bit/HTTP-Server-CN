# HTTP Server CN
This server runs on your localhost on port 90 on Linux OS

## Configuring Server
Run the script configure.sh  and refresh your terminal
```bash
./configure.sh
source ~/.bashrc
```


## Starting the server

```bash
start_server    
```
T
## Stopping the server
```bash
stop_server
```

## Restarting the server
```bash
restart_server
```

## A simple HTTP server in Python3
Test Suite -> 
### GET Request
1. Bad Request
2. Single GET
3. Version not supported
4. Bad Request in GET
5. 301 Request in GET
6. 300 Request in GET
7. 404 in GET
8. Condition GET -> 304
9. GET running succesfully 

### PUT Requests
1. Simple PUT Requests
2. 403 in PUT
3. If directory not present goes on creating it and makes a new resource

### POST Requests
1. application/x-www-form-urlencoded form type
2. multipart form data -> with images and text file
3. forbidden in post requests
4. Something written in post
5. 404 in post

### HEAD Requests 
1. Same like GET


### DELETE Requests 
1. Check if has file permissions
2. Otherwise delete
