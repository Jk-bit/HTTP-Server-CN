import configparser

config = configparser.ConfigParser()
config['DEFAULT'] = {
            'DirectoryRoot' : '/home/jatish/Academics/CN/Project/html',
            'AccessLogFile' : '/home/jatish/Academics/CN/Project/log/access.log',
            'KeepAliveConnections' : '30',
        }

config["index.html"] = {
            'Redirections' : "/redirected/index.html"
        }

with open('myserver1.conf', 'w') as configfile :
    config.write(configfile)
