
class Log :
    def __init__(self, client_addr, log_location) :
        self.access_log_file = log_location
        self.client_addr = client_addr
    def access_log(self, status_code, size_of_file, request, date, user_agent, location=None) :
        log = str(self.client_addr[0]) + " - - " 
        log += self.date_for_log(date)
        log += '"' + str(request) + '" '
        log += str(status_code) + " "
        log += str(size_of_file) + " - "
        if(location) : 
            log += str(location) + " "
        log += str(user_agent) + "\n"
        access_log = open("log/access.log", "a+")
        access_log.write(log)
        access_log.close()

    """ 
        Format of date_time in log file 
        [date/month/year:hh:mm:ss +0530]
    """
    def date_for_log(self, date) :
        date = date.split(" ")
        if(date[2] == "") :
            date.pop(2)
        return "[" + (date[2]) + "/" + (date[1]) + "/" + (date[5][:-1]) + ":" + date[3] + "+0530" + "] " 



