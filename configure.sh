#!/bin/bash
 
echo "alias start_server='python3 -u /home/jatish/Academics/CN/Project/tcp_connection.py 9000 2> ~/Academics/CN/Project/output.txt &  '" >> ~/.bashrc
echo "alias stop_server='sudo pkill -f tcp_connection.py 2> ~/Academics/CN/Project/output.txt'" >> ~/.bashrc
echo "restart_server() {                                                             
     stop_server                                                                
     start_server                                                               
 }   " >> ~/.bashrc
