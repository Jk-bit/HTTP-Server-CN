
import socket 
import threading
import os
import log
from stat import *
import sys
import json
import datetime
import email.utils as eut
from collections import OrderedDict
import shortuuid
import configparser
#TODO what is shortuuid
#TODO Cookie storing in file


port=9000
""" 
    Class Connection : takes the host and port of the server and starts listening
    /accepting the connection of clients takes the data/packets from the client
    and can also send. 
    
    Connection.start_server() : creates a socket instance and binds it to the 
    port given, and starts listening to the server
    Connection.accpet_conn() :accpets client connection
    Connection.recv_req() : Get the request packet/ data from the client
    
"""
class Connection :
    def __init__(self, host, port) :
        self.port = port
        self.host = host
        self.multiple_conn = 0

    def start_server(self) :
        self.server_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.server_socket.bind((self.host, self.port))
        self.server_socket.listen(1)

    def accept_conn(self) :
        self.server_socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        client_socket, client_addr = self.server_socket.accept()
        self.log = log.Log(client_addr, self.access_log_file)
        return client_socket

    def recv_req(self, client_socket) :
        data = client_socket.recv(8192).decode("iso-8859-1")
        pac = data.split('\r\n\r\n')[0]
        pac = pac.split('\r\n')
        for headers in pac :
            if ("Content-Length" in headers) :
                size = int(headers.split(': ')[1])
                size = size - 8192
                while(size > 0) :
                    data += client_socket.recv(8192).decode("iso-8859-1")
                    size = size - 8192

        data += " "
        return data;
        
    def get_clientaddr(self) :
        return self.client_addr

    def get_clientsocket(self) :
        return self.client_socket()


""" 
    class response_packet : it contains the info about status code and it's description
    in the init(constructor) of the class
"""
class response_packet() :
    
    def __init__(self) :
        self.status_code_desc = { "200" : "OK" , 
                "201" : "Created" ,
                "204" : "No-Content",
                "300" : "Multiple Choices",
                "301" : "Moved Premanently",
                "304" : "Not Modified",
                "404" : "Not Found" ,
                "412" : "Precondition Failed",
                "403" : "Forbidden",
                "400" : "Bad Request", 
                "405" : "Method Not Allowed",
                "505" : "HTTP Version not supported"
                }
        self.header = OrderedDict()
        self.header = {}
        self.header = {
                "Version" : "HTTP/1.1"  , 
                "Status" : " " , 
                "Date" : "" ,
                "Server" : "JK_Server (Ubuntu)" ,
                "Content-Length" : "",
                "Connection" : "close" ,
                "Allow" : "GET, PUT, HEAD" ,
                "Content-Language" : "US-en" ,
                "Content-Type" : "" , 
                "Message-body" : "", 
                "Location" : ""
                }

    """ 
        Date : Weekday, Day Month Year hh:mm:ss IST
        using the popen method which stores the command output in a file and
        returns th fd, reading from fd and then closing it
        date is the linux command which returns the date as
        Weekday Day Month hh:mm:ss IST Year
        parsing it we return it
    """
    def get_current_date(self) : 
        stream = os.popen("date")
        self.date = stream.read()
        stream.close()
        date = self.date.split(" ")
        date.insert(3, date.pop()[:-1])
        date[0] += ","
        date = ' '.join(date)
        return date

""" 
    http_server() : In this class all the methods of a basic http server are 
    encapsulated, it inherits the Connection and respnse_packet class for
    connection and for the status code respectively, 

    start_http_server() : It starts the http server with the given host and
    port in the constructor.
    accept_http_req() : In this method we take the request packet from the http
    client and respond them accordingly
    http_response() : it responds the request from the client
    get_response() : creates the response packet for a GET request
    status_code() : returns the status_code for the particular request
    parse() : parses the raw data sent by the http client
"""

class http_server(Connection, response_packet) :
   
    #http server gets binded to the respective host and the port 
    def __init__(self) :
        Connection.__init__(self, '', port)
        response_packet.__init__(self)
    
    #starts listening to the connections
    def start_http_server(self) :
        self.start_server()
        self.read_config()
        self.read_cookies()

    def read_cookies(self) :
        self.cookies_list = []
        with open("cookie.txt", "r") as cookie_file :
            cookies = cookie_file.read()
            self.cookies_list = cookies.split('\n')

   #reads the config file and stores the necessary fields in the variables
    def read_config(self) :
        #redirections stores the key as the file whose values has the multiple
        #redirections   
        self.redirections = {}
        config = configparser.ConfigParser()
        config.read('myserver1.conf')
        self.directoryroot = config['DEFAULT']['directoryroot']
        os.chdir(self.directoryroot)
        #reads the path of the log file
        self.access_log_file = config['DEFAULT']['accesslogfile']
        #maximum alive threads
        self.keep_alive_conn = int(config['DEFAULT']['keepaliveconnections'])
        #stores the data about redirections in self.redirections
        for key in config["redirections"] :
            self.redirections[key] = config["redirections"][key].split(', ')
    
    #recieves the requests from the client
    #and sends the response to the client according to the request
    def accept_http_req(self, client_socket) :
        #req is the data recieved from the client
        req = self.recv_req(client_socket)
        #responds accordingly to the request packet
        self.http_response(req, client_socket)
    
    #function to send the response according to the request
    def http_response(self, req, client_socket) :
        #if the request is nothing i.e it is a bad request
        if(req == None) :
            self.bad_request(client_socket, None, header_pac)
            return 
        request_pac, msg_body= self.parse(req)
        method = list(request_pac[0].split(' '))[0]
        version = list(request_pac[0].split(' '))[2]
        header = self.header.copy()
        if(not(self.cookie_check(request_pac))) :
            #TODO cookies in list
            while(1) :
                cookie = shortuuid.uuid()
                if(cookie not in self.cookies_list) :
                    break;
            header['Set-Cookie'] = cookie
            self.cookies_list.append(cookie)
            with open("cookie.txt", "a+") as cookie_file :
                cookie_file.write(cookie+"\n")
        else :
            cookie = None
        #try :
        if (version != "HTTP/1.1" ) :
                self.version_not_supported(client_socket, request_pac, header)
                return
        if(method == "GET") :
                self.get_response(client_socket, request_pac, msg_body, header)
                return
        elif(method == "PUT") :
                self.put_response(client_socket, request_pac, msg_body, header)
                return
        elif(method == "HEAD") :
                self.head_response(client_socket, request_pac, msg_body, header)
                return
        elif(method == "DELETE") :
                self.delete_response(client_socket, request_pac, msg_body, header)
                return
        elif(method == "POST") :
                self.post_response(client_socket, request_pac, msg_body, header)
                return
        else :
                self.method_not_allowed(client_socket, ' '.join(request_pac), header)
                return

        #except :
        #    self.bad_request(client_socket, request_pac, header)
    #checks if the header consists cookie if not then returns False
    
    def cookie_check(self, request_pac) :
        for header in request_pac :
            if("Cookie" in header) :
                return True
        return False

    #sends the packet by preparing the response packet
    
    def send_packet(self, client_socket, request, header_pac, image=None, entity=None, location=None) :
        header_pac['Content-Length'] = str(sys.getsizeof(entity))
        for header in header_pac :
            if(header == "Version") :
                response = header_pac[header] + " "
            elif(header == "Status") :
                response += header_pac[header] + " " + self.status_code_desc[header_pac[header]] + "\n"
            elif(header_pac[header] != "") :
                response += header + ": " + header_pac[header] + "\n"
        if(entity != None) :
            if(image) :
                response = response.encode()
                response += b"\n" + entity + b"\n"
            else:
                response += "\n" + entity + "\n"
                response = response.encode()
        else : 
            response = response.encode()
        stream = os.popen("date")
        date = stream.read()
        if(location) :
            self.log.access_log(header_pac["Status"], header_pac["Content-Length"], request, date, "Google-Chrome", location)
        else :
            self.log.access_log(header_pac["Status"], header_pac["Content-Length"], request, date, "Google-Chrome")
        client_socket.send(response)
    
    #form response 
    def form_response(self, method, request_uri, header_pac, message=None) :
            #A dictionary to store the form data and dumping in a json file
            form = {}    
            #if method is GET the form data is in the url itself
            if(method == "GET") :
                location = request_uri.split('?')[0]
                form_data = request_uri.split('?')[1]
            #in post the form data is in entity 
            else :
                location = request_uri
                form_data = ''.join(message)
            form_data = form_data.split('&')
            for data in form_data :
                form[data.split('=')[0]] = data.split('=')[1]
            header_pac['Status'] = "201"
            if(os.path.isfile(request_uri)) :
                with open(request_uri, "a+") as f :
                    json.dump(form, f)
                    f.write("\n")
                entity = "<h1>Form Created</h1>"
                header_pac["Content-Type"] = "text/html"
                self.send_packet(client_socket, method, header_pac, None, entity, location)
                return
            else :
                self.page_not_found(client_socket, request_uri, header_pac)

    #form multipart response
    def form_response_multipart(self, client_socket, method, request_uri, boundary, header_pac, msg_body) :
        form = {}
        msg_body = ''.join(msg_body)
        msg_body = '\r\n' + msg_body
        msg_body = msg_body.split(boundary + "\r\n")
        request_dir = request_uri.split('/')
        form_data = request_dir[-1]
        if(len(request_dir) != 1) :
            request_dir = '/'.join(request_dir[:-1])
            if(not(os.path.isdir(request_dir))) :
                self.page_not_found(client_socket, request_uri, header_pac)
                return
            else :
                os.chdir(request_dir)
        else :
            form_data = '/'.join(request_dir)
        for data in msg_body :
            if("Content-Disposition: form-data" in data) :
                fields = data.split('; ')
                if(len(fields) == 3) :
                    value =  fields[1].split('"')[1]
                    filename = fields[2].split('"')[1]
                    content = fields[2].split('"')[2]
                    form[value] = "file: " + filename 
                    if(not(os.path.isfile(filename))) :
                        with open(filename, "wb") as form_file :
                            form_file.write(content.encode("iso-8859-1"))
                        os.chmod(filename, 0o660)
                elif(len(fields) == 2) :
                    field = data.split('"')[1] 
                    value = data.split('"')[2]
                    num = value.find('\r\n')
                    value = value[:num]
                    form[field] = value 
        if(os.path.isfile(form_data)) :
            with open(form_data, "a+") as f :
                json.dump(form, f)
            header_pac["Status"] = "201"
            header_pac["Date"] = self.get_current_date()
            entity = "<h1>Form Created</h1>"
            header_pac["Content-Type"] = "text/html"
            os.chdir(self.directoryroot)
            self.send_packet(client_socket, method, header_pac, None, entity, request_uri)
            return

        else :
            self.page_not_found(client_socket, request_uri, header_pac)
            os.chdir(self.directoryroot)

    #responds to the get request
    def get_response(self, client_socket, request_pac, msg_body, header_pac) :
        if(request_pac == None) :
            self.bad_request(client_socket, request_pac, header_pac)
            return
        method, request_uri, version = request_pac[0].split(' ')
        #A '?' in request uri denotes that the data is form data
        if('?' in request_uri[1:]) :
            self.form_response(method, request_uri[1:], header_pac, None)
            return
        #if the file is not present either it has been moved from it location
        #or it has mulltiple choices
        if (not(os.path.isfile(request_uri[1:]))) :
            for i in self.redirections :
                if(request_uri[1:] == i ) :
                    if(len(self.redirections[i]) == 1) :
                        header_pac['Location'] = (self.redirections[i])[0]
                        header_pac['Status'] = "301"
                        self.send_packet(client_socket, request_pac[0], header_pac)
                        return

                    else :
                        header_pac['Status'] = "300"
                        msg_body = """\r\n<form method="GET"> """
                        for j in self.redirections[i] :
                            msg_body += """<input type="radio">""" + ''.join(j) + "<br>\n"
                        msg_body += """<input type="submit">Send<br></form> """
                        self.send_packet(client_socket, request_pac[0], header_pac, None, msg_body, None)
                        return
            self.page_not_found(client_socket, request_pac[0], header_pac)
        #if it is a conditional GET
        else :
            for headers in request_pac :
                if("If-Modified-Since:" in headers) :
                    date = (headers.split(':')[1:])
                    date = (':'.join(date))[1:]
                    date = eut.parsedate_to_datetime(date)
                    date = eut.localtime(date)
                    time = os.path.getmtime(request_uri[1:])
                    date_m = datetime.datetime.fromtimestamp(time)
                    date_m = eut.localtime(date_m)
                    date_now = eut.localtime(datetime.datetime.now())
                    if(date > date_now) :
                        header_pac['Status'] = "400"    
                        entity = "<h1>Bad Request</h1>"
                        header_pac["Content-Type"] = "text/html"
                        self.send_packet(client_socket, method, header_pac, None, entity, request_uri[1:])
                        return
                    elif(date_m < date) :
                        header_pac['Status'] = "304"
                        self.send_packet(client_socket, method, header_pac, None, entity, request_uri[1:])
                        return
                elif("If-Unmodified-Since" in headers) :
                    date = (headers.split(':')[1:])
                    date = (':'.join(date))[1:]
                    date = eut.parsedate_to_datetime(date)
                    date = eut.localtime(date)
                    time = os.path.getmtime(request_uri[1:])
                    date_m = datetime.datetime.fromtimestamp(time)
                    date_m = eut.localtime(date_m)
                    date_now = eut.localtime(datetime.datetime.now())
                    if(date_now < date) :
                        header_pac['Status'] = "400"
                        entity = "<h1>Bad Request</h1>"
                        header_pac["Content-Type"] = "text/html"
                        self.send_packet(client_socket, method,  header_pac, None, entity, request_uri[1:])
                        return
                    if(date_m > date) :
                        header_pac['Status'] = "412"
                        self.send_packet(client_socket, method, header_pac, None, None, request_uri[1:])
                        return
            header_pac["Status"] = "200"
            header_pac["Date"] = self.get_current_date() 
            header_pac["Content-Length"] = str(os.path.getsize(request_uri[1:]))
            if(request_uri[-4:] == ".png") :
                header_pac["Content-Type"] = "image/png"
                entity = open(request_uri[1:], "rb")
                send_packet(client_socket, method, header_pac, 1, entity.read())
                return
            elif(request_uri[-4:] == ".jpg") :
                header_pac["Content-Type"] = "image/jpg"
                entity = open(request_uri[1:], "rb")
                self.send_packet(client_socket, method, header_pac, 1, entity.read())
                return
            elif(request_uri[-5:] == ".jpeg") :
                header_pac["Content-Type"] = "image/jpeg"
                entity = open(request_uri[1:], "rb")
                self.send_packet(client_socket, method, header_pac, 1, entity.read())
                return
            elif(request_uri[-5:] == ".html" or request_uri[-4:] == ".php" or request_uri[-3:] == ".js" or request_uri[-4:] == ".css") :
                header_pac["Content-Type"] = "text/html"
                entity = open(request_uri[1:], "r")
                self.send_packet(client_socket, request_pac[0], header_pac, 0, entity.read())
                return 
            header_pac["Content-Type"] = "*/*" 
            entity = open(request_uri[1:], "r")
            self.send_packet(client_socket, request_pac[0], header_pac, 0, entity.read())
            return

    def put_response(self, client_socket, request_pac, msg_body, header_pac) :
    
        if(request_pac == None or msg_body == None) :
            self.bad_request(client_socket, request_pac, header_pac)
        method, request_uri, version = request_pac[0].split(' ')
        if(os.path.isfile(request_uri[1:])) :
            #stream = os.popen("ls -l" + " " + request_uri[1:])
            #write = stream.read()
            #print(write, write[8])
            if(os.access(request_uri[1:], os.W_OK)) :
                #os.remove(request_uri[1:])
                put_file = open(request_uri[1:], "wb")
                put_file.write((''.join(msg_body)).encode("iso-8859-1"))
                #os.chmod(request_uri[1:], S_IROTH | S_IWOTH | S_IWUSR | S_IRUSR | S_IWGRP | S_IRGRP) 
                entity = None
                header_pac["Status"] = "201"
                entity = "<h1>Resource created</h1>"
            else :
                header_pac["Status"] = "403"
                entity = "<h1>Forbidden</h1>"
                header_pac["Content-Type"] = "text/html"

        else :
            directory = (request_uri[1:]).split('/')
            for d in directory[:-1] :
                if(not(os.path.isdir(d))) :
                    os.mkdir(d)
                    os.chmod(d, S_IROTH | S_IXOTH | S_IWUSR | S_IRGRP | S_IRUSR | S_IXGRP | S_IXUSR)
                    os.chdir(d)
            os.chdir(self.directoryroot)
            put_file = open(request_uri[1:], "wb")
            put_file.write(''.join(msg_body).encode("iso-8859-1"))
            os.chmod(request_uri[1:], 0o666)
            header_pac["Status"] = "201"
            entity = "<h1>Resource created</h1>"
        header_pac["Date"] = self.get_current_date()
        self.send_packet(client_socket, request_pac[0], header_pac, None, entity, request_uri[1:])
        return

    def post_response(self, client_socket, request_pac, msg_body, header_pac) :
        if(request_pac == None or msg_body == None) :
            self.bad_request(client_socket, request_pac, header_pac)
        method, request_uri, version = request_pac[0].split(' ')
        if(os.path.isfile(request_uri[1:]) or os.path.isdir(request_uri[1:])) :
            
            stream = os.popen('ls -l ' + request_uri[1:])
            write = stream.read()
            write = write[8]
            for header in request_pac :
                if(header == "Content-Type: application/x-www-form-urlencoded" or header.split('?')[0] == "Content-Type: multipart/form-data") :
                    self.form_response("POST", request_uri[1:], header_pac, msg_body)
                    return
                elif(header.split(';')[0] == "Content-Type: multipart/form-data") :
                    boundary = (''.join(header.split(';')[1])).split('=')[1]
                    self.form_response_multipart(client_socket, request_pac[0], request_uri[1:], boundary, header_pac, msg_body)
                    return 
            if(write == "w") :
                post_file = open(request_uri[1:], "a+")
                post_file.write(''.join(msg_body)) 
                header_pac["Status"] = "201"
                header_pac["Date"] = self.get_current_date()
                self.send_packet(client_socket, "POST", header_pac)
            else :
                header_pac["Status"] = "403"
                header_pac["Date"] = self.get_current_date()
                self.send_packet(client_socket, ''.join(request_pac[0]), header_pac)
        else :
            self.page_not_found(client_socket, ''.join(request_pac[0]), header_pac)
            
    def head_response(self, client_socket, request_pac, msg_body, header_pac) :
        if(request_pac == None or msg_body == None) :
            self.bad_request(client_socket, request_pac, header_pac)
            return
        method, request_uri, version = request_pac[0].split(' ')
        if (not(os.path.isfile(request_uri[1:]))) :
            for i in self.redirections :
                if(request_uri[1:] == i ) :
                    if(len(self.redirections[i]) == 1) :
                        header_pac['Location'] = (self.redirections[i])[0]
                        header_pac['Status'] = "301"
                        self.send_packet(client_socket, request_pac[0], header_pac)
                        return
                    else :
                        #TODO
                        header_pac['Status'] = "300"
                        self.send_packet(client_socket, request_pac[0], header_pac)
                        return
            self.page_not_found(client_socket, request_pac[0], header_pac)
            return
        else :
            for headers in request_pac :
                if("If-Modified-Since:" in headers) :
                    date = (headers.split(':')[1:])
                    date = (':'.join(date))[1:]
                    date = eut.parsedate_to_datetime(date)
                    date = eut.localtime(date)
                    time = os.path.getmtime(request_uri[1:])
                    date_m = datetime.datetime.fromtimestamp(time)
                    date_m = eut.localtime(date_m)
                    date_now = eut.localtime(datetime.datetime.now())
                    if(date > date_now) :
                        header_pac['Status'] = "400"    
                        entity = "<h1>Bad Request</h1>"
                        header_pac["Content-Type"] = "text/html"
                        self.send_packet(client_socket, method, header_pac, None, entity, request_uri[1:])
                        return
                    elif(date_m < date) :
                        header_pac['Status'] = "304"
                        self.send_packet(client_socket, method, header_pac, None, entity, request_uri[1:])
                        return
                elif("If-Unmodified-Since" in headers) :
                    date = (headers.split(':')[1:])
                    date = (':'.join(date))[1:]
                    date = eut.parsedate_to_datetime(date)
                    date = eut.localtime(date)
                    time = os.path.getmtime(request_uri[1:])
                    date_m = datetime.datetime.fromtimestamp(time)
                    date_m = eut.localtime(date_m)
                    date_now = eut.localtime(datetime.datetime.now())
                    if(date_now < date) :
                        header_pac['Status'] = "400"
                        entity = "<h1>Bad Request</h1>"
                        header_pac["Content-Type"] = "text/html"
                        self.send_packet(client_socket, method,  header_pac, None, entity, request_uri[1:])
                        return
                    if(date_m > date) :
                        header_pac['Status'] = "412"
                        self.send_packet(client_socket, method, header_pac, None, None, request_uri[1:])
                        return
        header_pac["Status"] = "200"
        header_pac["Date"] = self.get_current_date()
        header_pac["Content-Length"] = str(os.path.getsize(request_uri[1:]))
        self.send_packet(client_socket, request_pac[0], header_pac)
        return
    
    def delete_response(self, client_socket, request_pac, msg_body, header_pac) :
        method, request_uri, version = request_pac[0].split(' ')
        if(request_pac == None or msg_body == None) :
            self.bad_request(client_socket, request_pac,header_pac)
            return
        if(os.path.isfile(request_uri[1:]) or os.path.isdir(request_uri[1:]) ) :
            
            stream = os.popen("ls -l " + request_uri[1:])
            write = stream.read()
            if(write[8] != "w") :
                header_pac["Status"] = "403"
                entity = "<h1>Forbidden</h1>"
                header_pac["Content-Type"] = "text/html"
            else :
                header_pac["Status"] = "204"
                os.remove(request_uri[1:])
        else :
            self.bad_request(client_socket, request_pac, header_pac)
            return
        header_pac["Date"] = self.get_current_date()
        self.send_packet(client_socket, request_pac[0], header_pac, None, None,request_uri[1:])

    def bad_request(self, client_socket, request_pac, header_pac) :
        header_pac["Status"] = "400"
        entity = "<h1>Bad Request</h1>"
        header_pac["Date"] = self.get_current_date()
        self.send_packet(client_socket, request_pac, header_pac, None, entity)

    def page_not_found(self, client_socket, request_uri,header_pac) :
        header_pac["Status"] = "404"
        header_pac["Date"] = self.get_current_date()
        entity = "<h1>Error 404 Not Found</h1>"
        #header_pac["Content-Location"] = request_uri.split(' ')[1]
        self.send_packet(client_socket, request_uri, header_pac, None, entity)

    def method_not_allowed(self, client_socket, request_pac, header_pac) :
        header_pac["Status"] = "405"
        entity = "<h1>Method Not Allowed</h1>"
        header_pac["Date"] = self.get_current_date()
        self.send_packet(client_socket, request_pac, header_pac, None, entity)

    def version_not_supported(self, client_socket, request_pac, header_pac) :
        header_pac["Status"] = "505"
        entity = "<h1>HTTP version is not supported on this server</h1>"
        self.send_packet(client_socket, request_pac, header_pac, None, entity)

    def parse(self, req) :
        try :
            request = req.split('\r\n\r\n')[0]
            request = request.split('\r\n')
            msg_body = req.split('\r\n\r\n')[1:]
            return request, msg_body
        except :
            self.bad_request(request_pac, client_socket)

lock = threading.Lock()
http = http_server()
http.start_http_server()
while(1) :
    client_socket = http.accept_conn()
    client_thread = threading.Thread(target=http.accept_http_req, args=(client_socket, ))
    while(http.keep_alive_conn < threading.active_count()) :
        pass
    client_thread.start()
    #print("THread count", threading.active_count())
    
    
